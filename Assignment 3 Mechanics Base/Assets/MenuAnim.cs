using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuAnim : MonoBehaviour {
    
    public bool player, queen;
    private Animator anim;
    public GameObject avatar;
    public GameObject bullet;
    private float MGFireTime = 0.075f;
    private float MGFireTimer;
    public GameObject muzzleFlash;

    private void Start() {
        anim = avatar.GetComponent<Animator>();
    }


    void Update() {
        switch (MenuMode.instance.currentMode) {
            case MenuMode.CurrentMenuMode.START:
                StartMenuBG();
                return;
            case MenuMode.CurrentMenuMode.DEAD:
                DeadMenuBG();
                return;
            case MenuMode.CurrentMenuMode.VICTORY:
                VictoryMenuBG();
                return;
        }
    }

    private void VictoryMenuBG() {
        if(player) {
            anim.SetBool("Dead", false);
             anim.SetBool("Dead", false);
            muzzleFlash.SetActive(true);

            if (Time.time > MGFireTimer) {
                Instantiate(bullet, muzzleFlash.transform.position, transform.rotation);
                MGFireTimer = Time.time + MGFireTime;
            }

            anim.SetBool("Shooting", true);
        } 
        if(queen) {
            anim.SetTrigger("Die");
            anim.SetBool("Dead", true);
            queen = false;
        }
    }

    private void DeadMenuBG() {
        if(player) {
            anim.SetBool("Shooting", false);
            anim.SetBool("Dead", true);
        } 
        if(queen) {
            anim.SetTrigger("Stab Attack");
        }
    }

    private void StartMenuBG() {
        if(player) {
            anim.SetBool("Dead", false);
            muzzleFlash.SetActive(true);

            if (Time.time > MGFireTimer) {
                Instantiate(bullet, muzzleFlash.transform.position, transform.rotation);
                MGFireTimer = Time.time + MGFireTime;
            }

            anim.SetBool("Shooting", true);
        } 
        if(queen) {
            anim.SetBool("Walk Forward", true);
        }
    }
}
