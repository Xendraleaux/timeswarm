using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lvl3ToggleTrigger : MonoBehaviour
{

    public Lvl3SpawnToggler spawnToggle;
    public int areaCode;
    public bool triggered;

    // Start is called before the first frame update
    void Start()
    {
        triggered = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (areaCode == 2 && triggered == false)
            {
                spawnToggle.Activate3();
                triggered = true;
            }
        }
    }
}
