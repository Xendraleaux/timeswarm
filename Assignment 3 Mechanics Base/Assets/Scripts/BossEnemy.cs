using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossEnemy : MonoBehaviour
{

    public GameObject player;
    public GameObject Portal;

    public float health = 10.0f;

    public float agroRange = 10.0f;
    public float damage = 5.0f;

    //Rotation vars
    public float rotationSpeed;
    private float adjRotSpeed;
    public Quaternion targetRotation;

    //Laser Damage
    public GameObject laser;
    public GameObject[] mainMuzzles;
    public GameObject[] secondaryMuzzles;
    private float laserTimer;
    private float laserTimer2;
    private float laserTime = 1.0f;
    private float laserTime2 = 0.25f;

    public bool enraged;
    private float enrageDuration = 15.0f;
    public float enrageCountdown;
    private int enrageCount;

    //Collision Damage
    private float damageTimer;
    private float damageTime = 0.5f;

    public GameObject burning;
    public GameObject explosion;

    public GameObject AmmoBox;

    public Transform ground;

    // Start is called before the first frame update
    void Start()
    {
        enraged = false;
        enrageCount = 0;
    }

    // Update is called once per frame
    void Update()
    {

        Behaviour();

        if ((health <= 7500 && enrageCount == 0) || (health <= 5000 && enrageCount == 1) || (health <= 2500 && enrageCount == 2))
        {
            enraged = true;
            enrageCount += 1;
            enrageCountdown = enrageDuration;
            Instantiate(AmmoBox, player.transform.position, player.transform.rotation);
        }

        if (enraged == true)
        {
            if (enrageCountdown > 0)
            {
                enrageCountdown -= Time.deltaTime;
            }
            else
            {
                enraged = false;
                enrageCountdown = 0.0f;
            }
        }
        
        
        //Kill check
        if (health <= 0)
        {
            Instantiate(explosion, ground.position, ground.rotation);
            Destroy(this.gameObject);
        }
    }

    void Behaviour()
    {
        if (!player)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
        else if (player && !GameManager.instance.playerDead)
        {
            //Rotate slowly towards player
            targetRotation = Quaternion.LookRotation(player.transform.position - transform.position);
            adjRotSpeed = Mathf.Min(rotationSpeed * Time.deltaTime, 1);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, adjRotSpeed);

            //Fire Laser
            if (enraged == false)
            {
                if (Time.time > laserTimer)
                {
                    for (int i = 0; i < mainMuzzles.Length; i++)
                    {
                        Instantiate(laser, mainMuzzles[i].transform.position, mainMuzzles[i].transform.rotation);
                    }
                    laserTimer = Time.time + laserTime;
                }
            }
            

            //enter enrage mode at different hp thresholds
            if (enraged == true)
            {
                if (Time.time > laserTimer2)
                {
                    for (int i = 0; i < secondaryMuzzles.Length; i++)
                    {
                        Instantiate(laser, secondaryMuzzles[i].transform.position, secondaryMuzzles[i].transform.rotation);
                    }
                    laserTimer2 = Time.time + laserTime2;
                }
            }
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.transform.tag == "Player" && Time.time > damageTimer)
        {
            collision.transform.GetComponent<PlayerAvatar>().TakeDamage(damage);
            damageTimer = Time.time + damageTime;
        }
    }

    public void TakeDamage(float thisDamage)
    {
        health -= thisDamage;
    }

}
