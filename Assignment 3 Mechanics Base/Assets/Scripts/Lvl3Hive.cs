using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lvl3Hive : MonoBehaviour
{
    public float health = 100.0f;

    public GameObject burning;
    public GameObject explosion;

    public Lvl3SpawnToggler spawnToggle;
    public int areaCode;

    public GameObject enemy1;
    public float spawnInterval;
    private float timer;

    public bool activated;

    // Start is called before the first frame update
    void Start()
    {
        activated = false;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > spawnInterval)
        {
            if (activated == true)
            {
                Instantiate(enemy1, this.transform.position, this.transform.rotation);
            }
            timer = 0.0f;
        }

        //Kill check - moved from takeDamage due to bug
        if (health <= 0)
        {
            Instantiate(explosion, transform.position, transform.rotation);
            if (areaCode == 1)
            {
                spawnToggle.Activate2();
            }
            else if (areaCode == 3)
            {
                spawnToggle.Activate4();
            }
            else if (areaCode == 4)
            {
                spawnToggle.ActivateBoss();
            }
            Destroy(this.gameObject);
        }
    }

    public void TakeDamage(float thisDamage)
    {
        health -= thisDamage;
    }
}
