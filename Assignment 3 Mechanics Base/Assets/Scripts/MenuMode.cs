using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuMode : MonoBehaviour {
    
    public enum CurrentMenuMode {
        START, DEAD, VICTORY
    }

    public CurrentMenuMode currentMode;

    public static MenuMode instance;

    private void Awake() {
        if (instance == null) {
            instance = this;
        }  else if (instance != this) {
            Destroy(gameObject);
        }
    }
    
    // Start is called before the first frame update
    void Start() {
        // currentMode = CurrentMenuMode.START;
        DontDestroyOnLoad(this);
    }
}
