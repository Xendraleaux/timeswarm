using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hive : MonoBehaviour
{
    public float health = 100.0f;

    public GameObject burning;
    public GameObject explosion;

    public SpawnerToggle spawnToggle;
    public int areaCode;

    public GameObject enemy1;
    public float spawnInterval;
    private float timer;

    public AudioSource spawnSound;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > spawnInterval)
        {
            spawnSound.Play();
            Instantiate(enemy1, this.transform.position, this.transform.rotation);
            timer = 0.0f;
        }

        //Kill check - moved from takeDamage due to bug
        if (health <= 0)
        {
            Instantiate(explosion, transform.position, transform.rotation);
            if (areaCode == 1)
            {
                spawnToggle.DeactivateEast();
            }
            else if (areaCode == 2)
            {
                spawnToggle.DeactivateNorth();
            }
            Destroy(this.gameObject);
        }
    }

    public void TakeDamage(float thisDamage)
    {
        health -= thisDamage;
    }
}
