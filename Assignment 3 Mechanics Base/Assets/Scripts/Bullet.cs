﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : Projectile {

    public override void OnTriggerEnter(Collider otherObject) {

        if (otherObject.tag == "Enemy")
        {
            otherObject.GetComponent<Enemy>().TakeDamage(damage);
            Instantiate(hitEffect, transform.position, transform.rotation);
            Destroy(this.gameObject);
        }
        else if (otherObject.tag == "BossEnemy")
        {
            otherObject.GetComponent<BossEnemy>().TakeDamage(damage);
            Instantiate(hitEffect, transform.position, transform.rotation);
            Destroy(this.gameObject);
        }
        else if (otherObject.tag == "Hive")
        {
            otherObject.GetComponent<Hive>().TakeDamage(damage);
            Instantiate(hitEffect, transform.position, transform.rotation);
            Destroy(this.gameObject);
        }
        else if (otherObject.tag == "Hive2")
        {
            otherObject.GetComponent<Lvl3Hive>().TakeDamage(damage);
            Instantiate(hitEffect, transform.position, transform.rotation);
            Destroy(this.gameObject);
        }
        else if (otherObject.tag == "Spawner")
        {
            otherObject.GetComponent<SpawnEnemy>().TakeDamage(damage);
            Instantiate(hitEffect, transform.position, transform.rotation);
            Destroy(this.gameObject);
        }
        else if (otherObject.tag == "Environment")
        {
            Instantiate(hitEffect, transform.position, transform.rotation);
            Destroy(this.gameObject);
        }
    }
}
