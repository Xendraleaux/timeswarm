using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSpawner : MonoBehaviour
{

    public GameObject Queen;
    public Transform QueenSpawnPoint;
    public bool bossSpawned;
    
    // Start is called before the first frame update
    void Start()
    {
        bossSpawned = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider otherObject)
    {
        if (otherObject.tag == "Player")
        {
            if (bossSpawned == false)
            {
                Instantiate(Queen, QueenSpawnPoint.position, QueenSpawnPoint.rotation);
                bossSpawned = true;
            }
        }
    }
}
