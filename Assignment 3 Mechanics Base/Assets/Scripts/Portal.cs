using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Portal : MonoBehaviour
{
    public GameObject[] hives;
    public float health;
    public float maxHealth = 100;
    public Image healthBar;

    // Start is called before the first frame update
    void Start()
    {
        health = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        healthBar.fillAmount = health / maxHealth;
        hives = GameObject.FindGameObjectsWithTag("Hive");
        if (hives.Length == 0) 
        {
            GameManager.instance.levelComplete = true;
        }
    }

    public void TakeDamage(float damage) 
    {
        health -= damage;
    }
}
