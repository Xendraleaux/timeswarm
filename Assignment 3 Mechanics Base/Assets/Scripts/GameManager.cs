﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    //Singleton Setup
    public static GameManager instance = null;

    public bool playerDead = false;

    public bool levelComplete = false;

    public string thisLevel;
    public int nextLevel;
    public MenuMode menuMode;


    // Awake Checks - Singleton setup
    void Awake() {

        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
    }

    // Use this for initialization
    void Start () {
        thisLevel = SceneManager.GetActiveScene().name;
        nextLevel = SceneManager.GetActiveScene().buildIndex + 1;
        menuMode = GameObject.FindGameObjectWithTag("MenuMode").GetComponent<MenuMode>();
	}
	
	// Update is called once per frame
	void Update () {
		if(playerDead) {
            menuMode.currentMode = MenuMode.CurrentMenuMode.DEAD;
            SceneManager.LoadScene(0);
        }
        if(levelComplete && thisLevel == "Level3_Ancient") {
            menuMode.currentMode = MenuMode.CurrentMenuMode.VICTORY;
            SceneManager.LoadScene(0);
        }
	}

    public IEnumerator LoadNextLevel() {

        yield return new WaitForSeconds(0);
        SceneManager.LoadScene(nextLevel);
    }


}
