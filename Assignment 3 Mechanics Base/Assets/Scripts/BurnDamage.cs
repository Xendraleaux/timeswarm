﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurnDamage : MonoBehaviour {

    public float damage = 10;

    public List<Transform> burnTargets = new List<Transform>();

    private float damageTime = 0.25f;
    private float damageTimer;

    // Start is called before the first frame update
    void Start(){
        if (transform.parent.tag == "Enemy" || transform.parent.tag == "Hive" || transform.parent.tag == "Spawner")
            burnTargets.Add(transform.parent);
    }

    // Update is called once per frame
    void Update(){
        if (Time.time > damageTimer) {
            foreach (Transform target in burnTargets) {
                if (target != null)
                    if (transform.parent.tag == "Hive")
                    {
                        target.GetComponent<Hive>().TakeDamage(damage);
                    }
                    else if (transform.parent.tag == "Hive2")
                    {
                        target.GetComponent<Lvl3Hive>().TakeDamage(damage);
                    }
                    else if (transform.parent.tag == "Enemy")
                    {
                        target.GetComponent<Enemy>().TakeDamage(damage);
                    }
                    else if (transform.parent.tag == "Spawner")
                    {
                        target.GetComponent<SpawnEnemy>().TakeDamage(damage);
                    }
                    else if (transform.parent.tag == "BossEnemy")
                    {
                        target.GetComponent<BossEnemy>().TakeDamage(damage);
                    }
            }
            damageTimer = Time.time + damageTime;
        }
    }

    void OnTriggerStay(Collider other) {
        if (other.gameObject.tag == "Enemy" || other.gameObject.tag == "Hive" || other.gameObject.tag == "Hive2" || other.gameObject.tag == "Spawner" || other.gameObject.tag == "BossEnemy") {
            if(!burnTargets.Contains(other.gameObject.transform))
                burnTargets.Add(other.gameObject.transform);
        }
    }

    void OnTriggerExit(Collider other) {
        if (other.gameObject.tag == "Enemy" || other.gameObject.tag == "Hive" || other.gameObject.tag == "Hive2" || other.gameObject.tag == "Spawner" || other.gameObject.tag == "BossEnemy") {
            burnTargets.Remove(other.gameObject.transform);
        }
    }
}
