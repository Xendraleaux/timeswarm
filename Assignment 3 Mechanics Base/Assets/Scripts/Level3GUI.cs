using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level3GUI : MonoBehaviour
{
    GameObject player;
    GameObject Queen;

    public BossSpawner QueenSpawnTrigger;

    public Slider healthbar;
    public Text ammoText;
    public Text fuelText;
    public Slider bossHealthbar;
    public GameObject levelCompleteText;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (!player)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            healthbar.value = 0;
        }
        else if (player)
        {
            healthbar.value = player.GetComponent<PlayerAvatar>().health;
            ammoText.text = "Ammo: " + player.GetComponent<PlayerAvatar>().ammo.ToString();
            fuelText.text = "Fuel: " + player.GetComponent<PlayerAvatar>().fuel.ToString();
        }

        if (!Queen)
        {
            Queen = GameObject.FindGameObjectWithTag("BossEnemy");
            if (QueenSpawnTrigger.bossSpawned == false)
            {
                bossHealthbar.value = 100;
            }
            else
            {
                bossHealthbar.value = 0;
            }     
        }
        else if (Queen)
        {
            bossHealthbar.value = (Queen.GetComponent<BossEnemy>().health) / 100;
        }

        //Level Status Text
        if (GameManager.instance.levelComplete)
            levelCompleteText.SetActive(true);

    }
}
