using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{
    public GameObject enemy1;
    public float spawnInterval;
    private float timer;

    public SpawnerToggle SpawnToggler;
    public int areaCode;

    public float health;

    public AudioSource spawnSound;

    // Start is called before the first frame update
    void Start()
    {
        spawnInterval = 5.0f;
        timer = 0.0f;
    }

    public void TakeDamage(float damage) 
    {
        health -= damage;
        if (health <= 0) 
        {
            Destroy(this.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > spawnInterval)
        {
            spawnSound.Play();
            Instantiate(enemy1, this.transform.position, this.transform.rotation);
            timer = 0.0f;
        }

        /*
        if (SpawnToggler.activateEast == true)
        {
        if (areaCode == 1)
            {
                if (timer > spawnInterval)
                {
                    Instantiate(enemy1, this.transform.position, this.transform.rotation);
                    timer = 0.0f;
                }
            }
        }
        else if (SpawnToggler.activateNorth == true)
        {
            if (areaCode == 2)
            {
                if (timer > spawnInterval)
                {
                    Instantiate(enemy1, this.transform.position, this.transform.rotation);
                    timer = 0.0f;
                }
            }
        //}
        */
        
    }
}
