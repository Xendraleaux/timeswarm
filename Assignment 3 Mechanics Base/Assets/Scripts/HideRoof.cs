using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideRoof : MonoBehaviour {
    
    public int visbleLayer, nonvisLayer;

    private void OnTriggerEnter(Collider other) {
        if(other.tag == "Player")
            this.gameObject.layer = nonvisLayer;
    }

    private void OnTriggerExit(Collider other) {
        if(other.tag == "Player")
            this.gameObject.layer = visbleLayer;
    }

}
