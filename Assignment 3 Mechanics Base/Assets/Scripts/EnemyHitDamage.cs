using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHitDamage : MonoBehaviour
{
    float damage = 5;
    float count = 0.5f;

    public GameObject impactParticle;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        count -= Time.deltaTime;
        if (count <= 0) 
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player") 
        {
            other.GetComponent<PlayerAvatar>().TakeDamage(damage);
            Instantiate(impactParticle, transform.position, transform.rotation);
            Destroy(this.gameObject);
        }
        else if (other.tag == "Portal")
        {
            other.GetComponent<Portal>().TakeDamage(damage);
            Instantiate(impactParticle, transform.position, transform.rotation);
            Destroy(this.gameObject);
        }
    }
}
