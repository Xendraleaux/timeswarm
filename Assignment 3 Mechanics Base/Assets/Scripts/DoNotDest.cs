using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoNotDest : MonoBehaviour {

    public static DoNotDest instance;
    
    private void Awake() {
        if (instance == null) {
            instance = this;
        }  else if (instance != this) {
            Destroy(gameObject);
        }
    }
    private void Start() {
        DontDestroyOnLoad(this);
    }

}
