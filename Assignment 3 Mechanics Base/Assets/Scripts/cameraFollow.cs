using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFollow : MonoBehaviour
{
    public GameObject follow;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 temp;
        temp.x = follow.transform.position.x;
        temp.z = follow.transform.position.z;
        temp.y = transform.position.y;
        transform.position = temp;
    }
}
