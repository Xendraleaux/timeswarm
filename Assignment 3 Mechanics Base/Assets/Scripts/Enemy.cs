﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour {

    NavMeshAgent agent;

    public GameObject player;
    public GameObject Portal;

    public float health = 10.0f;

    public float agroRange = 10.0f;
    public float damage = 5.0f;

    //Rotation vars
    public float rotationSpeed;
    private float adjRotSpeed;
    public Quaternion targetRotation;

    //Laser Damage
    public GameObject laser;
    private float laserTimer;
    private float laserTime = 1.0f;

    public GameObject hitbox;
    public Transform hitPos;

    //Collision Damage
    private float damageTimer;
    private float damageTime = 0.5f;

    public GameObject burning;
    public GameObject explosion;

    public GameObject AmmoBox;
    public GameObject FuelBox;
    public GameObject HPBox;
    private int dropType;

    public Transform ground;

    public Animator animator;

    public AudioClip destroyed;

    // Use this for initialization
    void Start () {
        agent = GetComponent<NavMeshAgent>();
        //animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update() {

        Behaviour();

        //Kill check - moved from takeDamage due to bug
        if (health <= 0) {
            AudioSource.PlayClipAtPoint(destroyed, transform.position, 1f);
            dropType = Random.Range(1, 15);
            if (dropType == 1 || dropType == 2)
            {
                Instantiate(AmmoBox, ground.position, ground.rotation);
            }
            else if (dropType == 3)
            {
                Instantiate(HPBox, ground.position, ground.rotation);
            }
            else if (dropType == 4)
            {
                Instantiate(FuelBox, ground.position, ground.rotation);
            }
            Instantiate(explosion, ground.position, ground.rotation);
            Destroy(this.gameObject);
        }
    }

    void Behaviour() {
        if (!player)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            Portal = GameObject.FindGameObjectWithTag("Portal");
        }
        else if (player && !GameManager.instance.playerDead && (GameManager.instance.thisLevel == "Level1_2XXX" || GameManager.instance.thisLevel == "Level2_21st")) {

            //Raycast in direction of Player
            RaycastHit hit;
            if (Physics.Raycast(transform.position, -(transform.position - player.transform.position).normalized, out hit, agroRange)) {

                //If Raycast hits player
                if (hit.transform.tag == "Player") {

                    Debug.DrawLine(transform.position, player.transform.position, Color.red);

                    //Rotate slowly towards player
                    targetRotation = Quaternion.LookRotation(player.transform.position - transform.position);
                    adjRotSpeed = Mathf.Min(rotationSpeed * Time.deltaTime, 1);
                    transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, adjRotSpeed);

                    //Move towards player
                    if (Vector3.Distance(player.transform.position, transform.position) >= 4) 
                    {
                        agent.SetDestination(player.transform.position);
                        animator.SetBool("Walk Forward", true);
                    }
                    //Stop if close to player
                    else if (Vector3.Distance(player.transform.position, transform.position) < 4) 
                    {
                        animator.SetBool("Walk Forward", false);
                        agent.SetDestination(transform.position);

                        //Fire Laser
                        if (Time.time > laserTimer)
                        {
                            int temp = Random.Range(1, 3);
                            if (temp == 1) 
                            {
                                animator.SetTrigger("Smash Attack");
                                Instantiate(hitbox, hitPos.position, hitPos.rotation);
                            }
                            else if (temp == 2)
                            {
                                animator.SetTrigger("Stab Attack");
                                Instantiate(hitbox, hitPos.position, hitPos.rotation);
                            }
                            laserTimer = Time.time + laserTime;
                        }

                    }
                }
                
            }
            else
            {
                //Rotate towards portal
                targetRotation = Quaternion.LookRotation(Portal.transform.position - transform.position);
                adjRotSpeed = Mathf.Min(rotationSpeed * Time.deltaTime, 1);
                transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, adjRotSpeed);

                //Move towards portal
                if (Vector3.Distance(Portal.transform.position, transform.position) >= 5)
                {
                    agent.SetDestination(Portal.transform.position);
                    animator.SetBool("Walk Forward", true);
                }
                //Stop if close to portal
                else if (Vector3.Distance(Portal.transform.position, transform.position) < 5)
                {
                    animator.SetBool("Walk Forward", false);
                    agent.SetDestination(transform.position);

                    //Fire Laser
                    if (Time.time > laserTimer)
                    {
                        int temp = Random.Range(1, 3);
                        if (temp == 1)
                        {
                            animator.SetTrigger("Smash Attack");
                            Instantiate(hitbox, hitPos.position, hitPos.rotation);
                        }
                        else if (temp == 2)
                        {
                            animator.SetTrigger("Stab Attack");
                            Instantiate(hitbox, hitPos.position, hitPos.rotation);
                        }
                        laserTimer = Time.time + laserTime;
                    }
                }                
            }
        }
        else if (player && !GameManager.instance.playerDead && GameManager.instance.thisLevel == "Level3_Ancient")
        {
            //Rotate slowly towards player
            targetRotation = Quaternion.LookRotation(player.transform.position - transform.position);
            adjRotSpeed = Mathf.Min(rotationSpeed * Time.deltaTime, 1);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, adjRotSpeed);

            //Move towards player
            if (Vector3.Distance(player.transform.position, transform.position) >= 5)
            {
                agent.SetDestination(player.transform.position);
                animator.SetBool("Walk Forward", true);
            }
            //Stop if close to player
            else if (Vector3.Distance(player.transform.position, transform.position) < 5)
            {
                animator.SetBool("Walk Forward", false);
                agent.SetDestination(transform.position);

                //Fire Laser
                if (Time.time > laserTimer)
                {
                    int temp = Random.Range(1, 3);
                    if (temp == 1)
                    {
                        animator.SetTrigger("Smash Attack");
                        Instantiate(hitbox, hitPos.position, hitPos.rotation);
                    }
                    else if (temp == 2)
                    {
                        animator.SetTrigger("Stab Attack");
                        Instantiate(hitbox, hitPos.position, hitPos.rotation);
                    }
                    laserTimer = Time.time + laserTime;
                }
            }
        }
    }


    private void OnCollisionStay(Collision collision) 
    {
        /*
        if (collision.transform.tag == "Player" && Time.time > damageTimer) {
            collision.transform.GetComponent<PlayerAvatar>().takeDamage(damage);
            damageTimer = Time.time + damageTime;
        }
        */
    }

    public void TakeDamage(float thisDamage) {

        health -= thisDamage;
    }

}
