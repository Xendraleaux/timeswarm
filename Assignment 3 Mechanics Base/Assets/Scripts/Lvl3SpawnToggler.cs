using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lvl3SpawnToggler : MonoBehaviour
{

    public GameObject Spawner;

    public Transform[] SpawnGroup2;
    public Transform[] SpawnGroup3;
    public Transform[] SpawnGroup4;

    public Lvl3Hive[] Hives;

    public GameObject BossGate;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Activate2()
    {
        for (int i = 0; i < SpawnGroup2.Length; i++)
        {
            Instantiate(Spawner, SpawnGroup2[i].position, SpawnGroup2[i].rotation);
        }
    }

    public void Activate3()
    {
        for (int i = 0; i < SpawnGroup3.Length; i++)
        {
            Instantiate(Spawner, SpawnGroup3[i].position, SpawnGroup3[i].rotation);
            Hives[1].activated = true;
        }
    }

    public void Activate4()
    {
        for (int i = 0; i < SpawnGroup4.Length; i++)
        {
            Instantiate(Spawner, SpawnGroup4[i].position, SpawnGroup4[i].rotation);
            Hives[2].activated = true;
        }
    }

    public void ActivateBoss()
    {
        Destroy(BossGate.gameObject);
    }
}
