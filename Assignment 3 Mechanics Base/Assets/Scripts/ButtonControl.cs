using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonControl : MonoBehaviour {
    
    public GameObject startScreen, deadScreen, victoryScreen;
    MenuMode menuMode;

    private void Start() {
        deadScreen.SetActive(false);
        victoryScreen.SetActive(false);
        startScreen.SetActive(false);
        menuMode = GameObject.FindGameObjectWithTag("MenuMode").GetComponent<MenuMode>();
        switch (menuMode.currentMode) {
            case MenuMode.CurrentMenuMode.START:
                startScreen.SetActive(true);
                return;
            case MenuMode.CurrentMenuMode.DEAD:
                deadScreen.SetActive(true);
                return;
            case MenuMode.CurrentMenuMode.VICTORY:
                victoryScreen.SetActive(true);
                return;
            default:
                startScreen.SetActive(true);
                return;
        }
    }

    public void OnClickStart() {
        SceneManager.LoadScene(1);
        startScreen.SetActive(false);
    }   

    public void OnClickExit() {
        Application.Quit();
    } 

    public void OnClickMenu() {
        startScreen.SetActive(true);
        deadScreen.SetActive(false);
        victoryScreen.SetActive(false);
        menuMode.currentMode = MenuMode.CurrentMenuMode.START;
    }

    public void OnDeath() {
        deadScreen.SetActive(false);
        startScreen.SetActive(false);
        victoryScreen.SetActive(false);
    }

    public void OnVictory() {
        victoryScreen.SetActive(true);
        deadScreen.SetActive(false);
        startScreen.SetActive(false);
    }
 
}
