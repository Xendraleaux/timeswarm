using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerToggle : MonoBehaviour
{
    public GameObject[] Hives;
    public GameObject[] EastSpawners;
    public bool activateEast;
    public GameObject[] NorthSpawners;
    public bool activateNorth;

    public GameObject[] Gates;


    // Start is called before the first frame update
    void Start()
    {
        activateEast = true;
        activateNorth = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DeactivateEast()
    {
        for (int i = 0; i < EastSpawners.Length; i++)
        {
            Destroy(Hives[0].gameObject);
            activateEast = false;
            Destroy(EastSpawners[i].gameObject);
            activateNorth = true;
        }
        Destroy(Gates[0].gameObject);
    }

    public void DeactivateNorth()
    {
        for (int i = 0; i < NorthSpawners.Length; i++)
        {
            Destroy(Hives[1].gameObject);
            activateNorth = false;
            Destroy(NorthSpawners[i].gameObject);
        }
        Destroy(Gates[1].gameObject);
    }
}
